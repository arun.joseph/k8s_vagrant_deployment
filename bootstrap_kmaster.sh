#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

# Initialize kubernetes master node with latest updates and upgrades
echo "[TASK 1] Initialize Master Node with latest upgrades"
apt-get update
apt-get upgrade

# Disable swap
echo "[TASK 2] Disable and turn off SWAP"
sed -i '/swap/d' /etc/fstab
swapoff -a

# Update hosts file
echo "[TASK 3] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
192.168.33.10 kmaster.k8s.int kmaster
192.168.33.11 kworker1.k8s.int kworker1
192.168.33.12 kworker2.k8s.int kworker2
EOF

# # Stop and disable firewalld
# echo "[TASK 4] Stop and Disable firewalld"
# systemctl disable firewalld >/dev/null 2>&1
# systemctl stop firewalld

# Install prerequisites
echo "[TASK 4] Install the prerequisites"
apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# Install docker container run time
echo "[TASK 5] Install docker container run time"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get -y install docker-ce=18.06.2~ce~3-0~ubuntu
docker version

# Setup docker daemon
echo "[TASK 6] Setup docker daemon"
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
mkdir -p /etc/systemd/system/docker.service.d
# Restart docker.
systemctl daemon-reload
systemctl restart docker

# Install kubernetes
echo "[TASK 7] Install kubernetes"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl

# Initialize kubernetes master with kubeadm
echo "[TASK 8] Initialize kubernetes master with kubeadm"
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=192.168.33.10
mkdir -p /home/vagrant/.kube
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown -R vagrant:vagrant /home/vagrant/.kube

# Install POD Network add-on (Flannel)
echo "[TASK 9] Install POD Network add-on (Flannel)"
su - vagrant -c "kubectl apply -f /vagrant/kube-flannel.yml"

# Get the cluster join command for the worker nodes in the sync folder
echo "[TASK 10] Get the cluster join command"
kubeadm token create --print-join-command > /vagrant/joincluster.sh
