# k8s_vagrant_deployment

## Introduction
The project is to deploy a vagrant based deployment into Virtual box VMs for setting up a kubernetes cluster.

## Requirements
* It is recommended to have a minimum of 16GB ram
* Atleast a Intel Core i5 or equivalent
* Vagrant needs to be installed
* Virtualbox needs to be installed

## Usage
* Clone the project to your local
* Access the project folder where the VagrantFile is and execute the below command

```
vagrant up
```

* Once the virtual machines are up you can access them with 

```
vagrant ssh kmaster
```

## Additional Information
### k8s_demo_yaml_files
The folder under the sync_data for k8s_demo_yaml_files provide a basic demo on the yaml files used to create kuberenetes assets like pods, deployments, replica sets and deployments.

You can deploy these with the kubectl commands. Sample command below to deploy a deployment

```
kubectl create -f deployment-definition.yml
```

### Docker Sample Voting Application
Docker provides a sample voting application developed on the microservice architecture based on different technologies using docker containers. The application contain the following components

* voting-app - Front-end application for casting votes
* redis - Redis for in memory storage
* voting-worker - Back end service to process data from redis and load data on DB
* postgres - DB for storing the voting data
* voting-result - Front-end application to talk to DB and publish results.

#### Deploy
To deploy the application on the pc follow below commands once docker is installed.

```
docker run -d --name=redis redis
docker run -d --name=db postgres:9.4
docker run -d --name=vote -p 5000:80 --link redis:redis dockersamples/examplevotingapp_vote
docker run -d --name=vote -p 5000:80 --link db:db dockersamples/examplevotingapp_result
docker run -d --name=vote -p 5000:80 --link db:db --link redis:redis dockersamples/examplevotingapp_worker
```
#### Cleanup Docker
After use you can cleanup your docket environment with the below command
```
docker container stop $(docker container ls -a -q) && docker system prune -a -f --volumes
```

